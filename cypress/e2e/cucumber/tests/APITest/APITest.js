import { Given,When,Then,And } from "cypress-cucumber-preprocessor/steps";
import api_page from "../../pages/APIPage/APIPage";


Then("check status and response status 200",()=>{
    api_page.checkAPI200();
    api_page.checkResponse200();
    api_page.checkNewAPI();
    api_page.checkAPI202();
})
