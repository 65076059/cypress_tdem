Feature: การสร้าง Custom Commands และ Debugging ใน Cypress

    Background: visit action page
      Given I visit action page
    
    Scenario: ทดสอบ custom command clickPosition ด้วยการคลิกที่มุมต่างๆ ของ action-canvas เช่น topLeft, center, bottomRight.
      When คลิกที่ topLeft ของ action-canvas
      And คลิกที่ center ของ action-canvas
      And คลิกที่ bottomRight ของ action-canvas