import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";
import todo_page from "../../pages/todoPage/todoPage";

Given("I visit ToDO page",()=>{
    todo_page.todoVisit();
})
Then("page have length",()=>{
    todo_page.haveLength(2);
})
And("page have text {string} and {string}",(text_first,text_last)=>{
    todo_page.haveText(0,text_first);
    todo_page.haveText(1,text_last);
    todo_page.haveTextFirst(text_first);
    todo_page.haveTextLast(text_last);
})
When("Add New item {string}",(newItem)=>{
    todo_page.addNewItem(newItem)
})
Then("Add New item {string} completed",(newItem)=>{
    todo_page.checkAddNewItem(newItem)
})

When("Checkbox {string} is parents",(item)=>{
    todo_page.checkBoxItem(item);
})

Then("Check {string} isChecked",(item)=>{
    todo_page.verifyCheckBoxItem(item);
})

