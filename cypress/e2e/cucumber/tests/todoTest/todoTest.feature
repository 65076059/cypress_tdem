Feature: Test ToDO Cypress

    Background: Visit page cypress ToDO
      Given I visit ToDO page

    Scenario Outline: Displays two todo items by default
      Then page have length 
      And page have text "<text_first>" and "<text_last>"
    Examples:
    |text_first|text_last|
    |Pay electric bill|Walk the dog|

    Scenario Outline: Can add new todo items
      When Add New item "<newItem>"
      Then Add New item "<newItem>" completed
    Examples:
    |newItem|
    |Feed the cat|
    |Feed the dog|

    Scenario Outline: can check off an item as completed
      When Checkbox "<item>" is parents
      Then Check "<item>" isChecked
    Examples:
    |item|
    |Pay electric bill|

    
