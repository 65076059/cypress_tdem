import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import tmobility_page from "../../pages/tmobilityPage/tmobilityPage";

const randomNumber = Math.floor(Math.random() * 1000);
const mockToken_random = `mock-token-0${randomNumber.toString().padStart(3, '0')}`;

beforeEach(() => {
    // cy.viewport('iphone-x');
    localStorage.setItem('mockToken',mockToken_random);
});
afterEach(() => {
    // cy.clearLocalStorage();
    // cy.clearCookies();
});

Given('I navigate to T-mobility line liff', () => {
    tmobility_page.tmobilityVisit();
});
When('I visit T-mobility line liff', () => { });
