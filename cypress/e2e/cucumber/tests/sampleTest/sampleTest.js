import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import sample_page from "../../pages/samplePage/samplePage";

Given("I visit demoqa {string}",(url)=>{
    sample_page.demoVisit(url);
});
Given("I visit tab {string}",(text)=>{
    sample_page.clickMenu(text);
    sample_page.checkPage(text);
});
When("I input Full Name",()=>{
    sample_page.inputFullName("Automation Testing");
});
And("I input Email",()=>{
    sample_page.inputEmail("test@gmail.com");
});
And("I input Current Address",()=>{
    sample_page.inputCurrentAddress("Text Current Address");
});
And("I input Permanent Address",()=>{
    sample_page.inputPermanentAddress("Text Permanent Address")
});
And("I click submit",()=>{
    sample_page.btnSubmit();
    cy.wait(500);
});
Then("Data input page is correct",()=>{
    sample_page.verifyFullName("Name:Automation Testing");
    sample_page.verifyEmail("Email:test@gmail.com");
    sample_page.verifyCurrentAddress("Current Address :Text Current Address ");
    sample_page.verifyPermanentAddress("Permananet Address :Text Permanent Address");
});


//LAB02 : Test Check Box
When("I checked Desktop and unchecked Desktop",()=>{
    sample_page.clickCollapse();
    sample_page.checkBoxDesktop();
});
And("I checked Documents and unchecked Documents",()=>{
    sample_page.checkBoxDocuments();
});
And("I checked Downloads and unchecked Downloads",()=>{
    sample_page.checkBoxDownloads();
});
And("I checked Home all",()=>{
    sample_page.checkBoxHome();
});
Then("View Data All isChecked",()=>{
    sample_page.verifyChecked();
    sample_page.expectItem();
    sample_page.checkAPI();
});

// Lab 3
When("I select {string}", (comment)=>{
    sample_page.conditionCheckComment(comment);
})

Then("I see selected {string} is correct",(comment)=>{
    sample_page.conditionIsCheck(comment);
})

// Lab 4

When("I click double click me button", ()=>{
    sample_page.doubleClick();
})

Then("I see result {string} message", (button)=>{
    sample_page.conditionMessageButton(button);
})

When("I click {string}", (button)=>{
    sample_page.conditionClickButton(button);
})