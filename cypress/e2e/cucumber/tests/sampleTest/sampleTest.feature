Feature: LAB and Home Work
    Background: Visit to demoqa
        Given I visit demoqa "https://demoqa.com/elements"
    
    Scenario: LAB01 : Test Text Box
        Given I visit tab "Text Box"
        When I input Full Name
        And I input Email
        And I input Current Address
        And I input Permanent Address
        And I click submit
        Then Data input page is correct

    Scenario: LAB02 : Test Check Box
        Given I visit tab "Check Box"
        When I checked Desktop and unchecked Desktop
        And I checked Documents and unchecked Documents
        And I checked Downloads and unchecked Downloads
        And I checked Home all
        Then View Data All isChecked

    Scenario Outline: LAB03 : Test Radio Button
        Given I visit tab "Radio Button"
        When I select "<sample>"
        Then I see selected "<sample>" is correct
    Examples:
    |sample|
    |Yes|
    |Impressive|
    |No|

    Scenario: LAB04 : Test Button Click
        Given I visit tab "Buttons"
        When I click double click me button
        Then I see result
    
    Scenario Outline: LAB04 : Test Button 2
        Given I visit tab "Buttons"
        When I click "<button>"
        Then I see result "<button>" message
    Examples:
    |button|
    |Double Click Me|
    |Right Click Me|
    |Click Me|








