import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"
import login from "../../pages/loginPage/loginPage"

Given("I navigate to the Website", () => {
    login.visitURL();
});

When("I entered valid credential", (datatable) => {
    datatable.hashes().forEach((element) => {
        login.enterLogin(element.usename, element.password);
    });
});

When("I entered usename and password", () => {
    login.enterLogin("standard_user", "secret_sauce");
});

When("I entered {string} and input {string}", (usename,password) => {
    login.enterLogin(usename, password);
});

And("User click on sign in button", () => {
    login.clickSubmitButton();
});

Then("Validate the title after login", () => {
    login.title();
});

