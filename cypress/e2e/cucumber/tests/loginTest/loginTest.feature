Feature: I want to login into the site with valid data

  Background: Navigate to the Website
    Given I navigate to the Website

  Scenario: Login Success for data 01
    When I entered valid credential
      | usename       | password |
      | standard_user | secret_sauce |
    And User click on sign in button
    Then Validate the title after login

  Scenario: Login Success for data 02
    When I entered usename and password
    And User click on sign in button
    Then Validate the title after login

  Scenario Outline: Login Success for data 03
    When I entered "<usename>" and input "<password>"
    And User click on sign in button
    Then Validate the title after login
  Examples: 
    | usename | password |
    | standard_user | secret_sauce |
    | locked_out_user | secret_sauce |
    | problem_user | secret_sauce |