import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import checked_page from "../../pages/checkedPage/checkedPage";

And("I checked pay electric bill",() =>{
    checked_page.checkbox("Pay electric bill");
});
When("Check Active",() =>{
    checked_page.clickText("Active");
});
And("I see Walk the dog is active",() =>{
    checked_page.haveText("Walk the dog");
});
Then("I see Pay electric bill not exist",() =>{
    checked_page.containNotExist("Pay electric bill")
});