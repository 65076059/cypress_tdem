class APIPage{

    checkAPI200() {
        cy.request('https://a27fd959435d4456859adacecf325223.api.mockbin.io/')
      .then((response) => {
        expect(response.status).to.eq(200);
      });
    }
    checkAPI201() {
        cy.request('https://e606e1dd9487406b88364ddef8f44bda.api.mockbin.io/')
      .then((response) => {
        expect(response.status).to.eq(201);
      });
    }
    checkResponse200() {
        cy.request('https://a27fd959435d4456859adacecf325223.api.mockbin.io/')
        .then((response) => {
          expect(response.body).to.have.property('message', 'Hello World');
        });
    }
    checkNewAPI(){
        cy.request({
            url: 'https://api-dev-sm-t-mobility.toyota.co.th/driver/busroute',
            failOnStatusCode: false
          })
          .then((response) => {
            expect(response.status).to.eq(404);
            expect(response.body).to.deep.include.members([
                { "routeName": "Yellow_Line" },
                { "routeName": "Green_Line" }
              ]);
            
          });
    }

    checkAPI202(){
        cy.request('https://eeb4a61c0c464d94b567daac3e84e359.api.mockbin.io/').then((response) => {
            // Assert that the HTTP status code is 200
            expect(response.status).to.eq(202);
      
            // Assert the structure of the response body
            expect(response.body).to.have.all.keys('page', 'per_page', 'total', 'total_pages', 'data', 'support');
            
            // Assert the pagination details
            expect(response.body.page).to.eq(2);
            expect(response.body.per_page).to.eq(6);
            expect(response.body.total).to.eq(12);
            expect(response.body.total_pages).to.eq(2);
      
            // Assert the length and structure of the data array
            expect(response.body.data).to.be.an('array').that.has.lengthOf(6);
            
            response.body.data.forEach((user) => {
              expect(user).to.include.keys('id', 'email', 'first_name', 'last_name', 'avatar');
            });
      
            // Optionally, assert the content of one or more users
            expect(response.body.data[0]).to.deep.include({
              id: 7,
              email: 'michael.lawson@reqres.in',
              first_name: 'Michael',
              last_name: 'Lawson',
              avatar: 'https://reqres.in/img/faces/7-image.jpg'
            });
      
            // Assert the support object
            expect(response.body.support).to.deep.include({
              url: "https://reqres.in/#support-heading",
              text: "To keep ReqRes free, contributions towards server costs are appreciated!"
            });
          });
          
    }
}
const api_page = new APIPage();
export default api_page;