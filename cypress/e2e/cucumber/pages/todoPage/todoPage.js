class todoPage {

    todoVisit() {
        cy.visit('https://example.cypress.io/todo');
    }
    haveLength(length) {
        cy.get('.todo-list li').should('have.length', length);
    }
    haveText(index,text){
        cy.get('.todo-list li').eq(index).should('have.text', text);
    }
    haveTextFirst(text_first) {
        cy.get('.todo-list li').eq(0).should('have.text', text_first);
    }
    haveTextLast(text_last) {
        cy.get('.todo-list li').eq(1).should('have.text', text_last);
    }
    addNewItem(newItem) {
        cy.get('[data-test=new-todo]').type(`${newItem}{enter}`);
    }
    checkAddNewItem(newItem) {
        cy.get('.todo-list li')
            .should('have.length', 3)
            .last()
            .should('have.text', newItem);
    }
    checkBoxItem(item) {
        cy.contains(item)
            .parent()
            .find('input[type=checkbox]')
            .check();
    }
    verifyCheckBoxItem(item){
        cy.contains(item)
        .parents('li')
        .should('have.class', 'completed');
    }

}
const todo_page = new todoPage();
export default todo_page;