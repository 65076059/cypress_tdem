class LoginPage{

    visitURL(){
        cy.visit("https://www.saucedemo.com/");
    }

    enterLogin(username,password){
        cy.get('[data-test="username"]').type(username);
        cy.get('[data-test="password"]').type(password);
    }

    clickSubmitButton(){
        cy.get('[data-test="login-button"]').click();
    }

    title(){
        cy.get('.title').contains("Products");
    }

}

const login = new LoginPage();
export default login;
