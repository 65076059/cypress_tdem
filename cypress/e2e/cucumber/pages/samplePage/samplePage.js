class samplePage {

    demoVisit(url) {
        cy.visit(url);
    }
    clickMenu(text) {
        cy.contains(text).click();
        // cy.get(':nth-child(1) > .element-list > .menu-list > #item-0').contains(text).click();
    }

    checkPage(text) {
        cy.get('.text-center').contains(text).should('have.text', text);
    }

    inputFullName(name) {
        cy.get('#userName').type(name);
    }

    inputEmail(email) {
        cy.get('#userEmail').type(email);
    }

    inputCurrentAddress(currentAddress) {
        cy.get('#currentAddress').type(currentAddress);
    }

    inputPermanentAddress(permanentAddress) {
        cy.get('#permanentAddress').type(permanentAddress);
    }

    btnSubmit() {
        cy.get('#submit').click();
    }

    verifyFullName(name) {
        cy.get('#name').should('be.visible');
        cy.contains(name).should('have.text', name);
    }

    verifyEmail(email) {
        cy.get('#email').should('be.visible');
        cy.contains(email).should('have.text', email);
    }
    verifyCurrentAddress(currentAddress) {
        cy.get('.border > #currentAddress').should('be.visible');
        cy.contains(currentAddress).should('have.text', currentAddress);
    }
    verifyPermanentAddress(permanentAddress) {
        cy.get('.border > #permanentAddress').should('be.visible');
        cy.contains(permanentAddress).should('have.text', permanentAddress);
    }

    clickCollapse() {
        cy.get('.rct-collapse').click();
    }
    checkBoxHome() {
        cy.get('#tree-node-home').check({ force: true });
    }
    checkBoxDesktop() {
        cy.get('#tree-node-desktop').check({ force: true });
        cy.get('#tree-node-desktop').uncheck({ force: true });
        cy.get('#tree-node-desktop').should('not.be.checked');
    }
    checkBoxDocuments() {
        cy.get('#tree-node-documents').check({ force: true });
        cy.get('#tree-node-documents').uncheck({ force: true });
        cy.get('#tree-node-documents').should('not.be.checked');

    }
    checkBoxDownloads() {
        cy.get('#tree-node-downloads').check({ force: true });
        cy.get('#tree-node-downloads').uncheck({ force: true });
        cy.get('#tree-node-downloads').should('not.be.checked');
    }

    verifyChecked() {
        cy.get('#tree-node-home').should('be.checked');
        cy.get('#tree-node-downloads').should('be.checked');
        cy.get('#tree-node-documents').should('be.checked');
        cy.get('#tree-node-desktop').should('be.checked');
    }

    expectItem() {
        const expectedItems = ['home', 'desktop', 'notes', 'commands', 'documents', 'workspace', 'react', 'angular', 'veu', 'office', 'public', 'private', 'classified', 'general', 'downloads', 'wordFile', 'excelFile'];
        expectedItems.forEach(item => {
            cy.get('#result').should('contain.text', item);
        });
    }


    //lab 3



















    conditionCheckComment(comment) {
        if (comment == "Yes") {
            cy.get('#yesRadio').should('be.enabled').check({ force: true });
        } else if (comment == "Impressive") {
            cy.get('#impressiveRadio').should('be.enabled').check({ force: true });
        } else if (comment == "No") {
            cy.get('#noRadio').then(($radio) => {
                if ($radio.is(':enabled')) {
                    cy.get('#noRadio').check({ force: true });
                } else {
                    cy.log('Radio button is disabled');
                }
            });
        }

    }

    conditionIsCheck(comment) {
        if (comment == "Yes") {
            cy.get('#yesRadio').should('be.checked');
            cy.get('.text-success').contains('Yes');
        } else if (comment == "Impressive") {
            cy.get('#impressiveRadio').should('be.checked');
            cy.get('.text-success').contains('Impressive');
        } else if (comment == "No") {
            cy.get('#noRadio').should('not.be.checked');
        }
    }



    //Lab 4
    doubleClick() {
        cy.get('#doubleClickBtn').dblclick();
    }

    conditionClickButton(button) {
        if (button == "Double Click Me") {
            cy.get('#doubleClickBtn').dblclick();
        } else if (button == "Right Click Me") {
            cy.get('#rightClickBtn').rightclick();
        } else if (button == "Click Me") {
            cy.get('.col-md-6 > :nth-child(2) > :nth-child(4)').contains(button).click();
        }
    }

    conditionMessageButton(button) {
        if (button == "Double Click Me") {
            cy.get('#doubleClickMessage').contains('You have done a double click');
        } else if (button == "Right Click Me") {
            cy.get('#rightClickMessage').contains('You have done a right click');
        } else if (button == "Click Me") {
            cy.get('#dynamicClickMessage').contains('You have done a dynamic click');
        }
    }

    checkAPI() {
        // Perform a GET request to the API
        cy.request('https://eeb4a61c0c464d94b567daac3e84e359.api.mockbin.io/').then((response) => {
            // Assert that the HTTP status code is 200
            expect(response.status).to.eq(200);

            // Assert the structure of the response body
            expect(response.body).to.have.all.keys('page', 'per_page', 'total', 'total_pages', 'data', 'support');

            // Assert the pagination details
            expect(response.body.page).to.eq(2);
            expect(response.body.per_page).to.eq(6);
            expect(response.body.total).to.eq(12);
            expect(response.body.total_pages).to.eq(2);

            // Assert the length and structure of the data array
            expect(response.body.data).to.be.an('array').that.has.lengthOf(6);
            response.body.data.forEach((user) => {
                expect(user).to.include.keys('id', 'email', 'first_name', 'last_name', 'avatar');
            });

            // Optionally, assert the content of one or more users
            expect(response.body.data[0]).to.deep.include({
                id: 7,
                email: 'michael.lawson@reqres.in',
                first_name: 'Michael',
                last_name: 'Lawson',
                avatar: 'https://reqres.in/img/faces/7-image.jpg'
            });

            // Assert the support object
            expect(response.body.support).to.deep.include({
                url: "https://reqres.in/#support-heading",
                text: "To keep ReqRes free, contributions towards server costs are appreciated!"
            });
        });
    }




}
const sample_page = new samplePage();
export default sample_page;