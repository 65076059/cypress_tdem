class checkedPage{

    checkbox(text){
        cy.contains(text)
        .parent()
        .find('input[type=checkbox]')
        .check()
    }

    clickText(text){
        cy.contains(text).click();
    }

    haveText(text){
        cy.get('.todo-list li')
        .should('have.length', 1)
        .first()
        .should('have.text', text);
    }

    containNotExist(text){
        cy.contains(text).should('not.exist');
    }

}
const checked_page = new checkedPage();
export default checked_page;